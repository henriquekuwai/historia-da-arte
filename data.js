window.json = window.json || {};
window.json.quadros = [
    {
        title: 'Retrato de senhora',
        author: 'Henrique Bernardelli',
        year: 1895,
        from: 'americas',
        img: 'retrato-de-senhora.jpg',
        infos: {
            linear: 1,
            pictural: 0,

            plano: 0,
            profundidade: 1,

            aberta: 0,
            fechada: 1,

            plural: 1,
            unitaria: 0,

            absoluta: 0,
            relativa: 1
        }
    },
    {
        title: 'Primavera',
        author: 'Sandro Botticelli',
        year: 1482,
        from: 'europa',
        img: 'primavera.jpg',
        infos: {
            linear: 1,
            pictural: 0,

            plano: 0,
            profundidade: 1,

            aberta: 0,
            fechada: 1,

            plural: 1,
            unitaria: 0,

            absoluta: 0,
            relativa: 1
        }
    },
    {
        title: 'Partida da Monção',
        author: 'Almeida Júnior',
        year: 1897,
        from: 'americas',
        img: 'partida-da-moncao.jpg',
        infos: {
            linear: 1,
            pictural: 0,

            plano: 1,
            profundidade: 0,

            aberta: 0,
            fechada: 1,

            plural: 1,
            unitaria: 0,

            absoluta: 1,
            relativa: 0
        }
    },
    {
        title: 'A Providência Guia Cabral',
        author: 'Eliseu Visconti',
        year: 1899,
        from: 'americas',
        img: 'a-providencia-guia-cabral.jpg',
        infos: {
            linear: 0,
            pictural: 1,

            plano: 0,
            profundidade: 1,

            aberta: 0,
            fechada: 1,

            plural: 0,
            unitaria: 1,

            absoluta: 1,
            relativa: 0
        }
    },
    {
        title: 'Caçador de Leões',
        author: 'O Senhor Pertuiset',
        year: 1881,
        from: 'europa',
        img: 'cacador-de-leoes.jpg',
        infos: {
            linear: 0,
            pictural: 1,

            plano: 0,
            profundidade: 1,

            aberta: 1,
            fechada: 0,

            plural: 0,
            unitaria: 1,

            absoluta: 0,
            relativa: 1
        }
    },
    {
        title: 'Caipira Picando Fumo',
        author: 'Almeida Júnior',
        year: 1893,
        from: 'americas',
        img: 'caipira-picando-fumo.jpg',
        infos: {
            linear: 1,
            pictural: 0,

            plano: 1,
            profundidade: 0,

            aberta: 0,
            fechada: 1,

            plural: 1,
            unitaria: 0,

            absoluta: 1,
            relativa: 0
        }
    },
    {
        title: 'Monalisa',
        author: 'Leonardo Da Vinci',
        year: 1503,
        from: 'europa',
        img: 'monalisa.jpg',
        infos: {
            linear: 0,
            pictural: 1,

            plano: 0,
            profundidade: 1,

            aberta: 1,
            fechada: 0,

            plural: 0,
            unitaria: 1,

            absoluta: 0,
            relativa: 1
        }
    }
];

window.json.home = {
    continentes: [{
        id: 'asia',
        name: 'Ásia',
        destino: '#escolha',
        specialClass: 'continente',
        bg: 'asia.jpg',
        color: '#FFF'
    },
    {
        id: 'americas',
        name: 'Américas',
        destino: '#escolha',
        specialClass: 'continente',
        bg: 'americas.jpg',
        color: '#FFF'
    },
    {
        id: 'europa',
        name: 'Europa',
        destino: '#escolha',
        specialClass: 'continente',
        bg: 'europa.jpg',
        color: '#FFF'
    },
    {
        id: 'africa',
        name: 'África',
        destino: '#escolha',
        specialClass: 'continente',
        bg: 'africa.jpg',
        color: '#FFF'
    },
    {
        id: 'oceania',
        name: 'Oceania',
        destino: '#escolha',
        specialClass: 'continente',
        bg: 'oceania.jpg',
        color: '#FFF'
    }]
};