function getQueryParams(a){var a=a.split("+").join(" "),d={},c,b=/[?&]?([^=]+)=([^&]*)/g;while(c=b.exec(a)){d[decodeURIComponent(c[1])]=decodeURIComponent(c[2])}return d};
    /* https://gist.github.com/jeffdrumgod/5841191 */
    if("function"!=typeof(String.prototype.replaceSpecialChars)) String.prototype.replaceSpecialChars=function(){var _replace={"ç":"c","æ":"ae","œ":"oe","á":"a","é":"e","í":"i","ó":"o","ú":"u","à":"a","è":"e","ì":"i","ò":"o","ù":"u","ä":"a","ë":"e","ï":"i","ö":"o","ü":"u","ÿ":"y","â":"a","ê":"e","î":"i","ô":"o","û":"u","å":"a","ã":"a","ø":"o","õ":"o","u":"u","Á":"A","É":"E","Í":"I","Ó":"O","Ú":"U","Ê":"E","Ô":"O","Ü":"U","Ã":"A","Õ":"O","À":"A","Ç":"C"};return this.replace(/[à-ú]/g,function(a){if(typeof(_replace[a])!="undefined") return _replace[a]; return a;});};
var QUERY_PARAMS = getQueryParams(window.location.href.split('#')[0].split('?')[1] || '')

function marginBottom() {
    var height = $('.quizinfo-wrapper').outerHeight();
    $('body').css('margin-bottom', height);
}

$(document).ready(function () {

    window.router = new Router();

    $('.btn-voltar').on('click', function (e) {
        e.preventDefault();
        router.goTo($(this).attr('href'), $(this), $(this).attr('data-before'));
    });

    marginBottom();
    $(window).resize(function () {
        marginBottom()
    });

});