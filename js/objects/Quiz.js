function Quiz(options) {
    var that = {
        state: 'idle',
        step: 1,
        atual: 0,
        questions: [],
        answered: []
    };
    var default_options = {
        pointsPerCorrect: 10,
        losePoints: 2,
        timeNextQuestion: 2000,
        firstText: 'Escolha uma opção por linha:',
        tryAgainText: 'Tente novamente, valendo {{pointsToWin}} pontos:',

        selectors: {
            mainTitle: '.main-title',

            subTitle: '.quadro-options .subtitle',

            quizWrapper: '.quiz-wrapper',
            quizInfo: '.quizinfo-wrapper',
            quizLivesTooltip: '.quizinfo-wrapper .lives-wrapper .tooltip',
            quizLives: '.quizinfo-wrapper .lives',
            quizPoints: '.quizinfo-wrapper .pontos',
            quizContinente: '.quizinfo-wrapper .continente',
            quizAtual: '.quizinfo-wrapper .atual',
            quizTotal: '.quizinfo-wrapper .total',
            btnNext: '.quizinfo-wrapper .botao',

            successModal: '.success-modal',

            quadroWrapper: '.quadro-wrapper',
            errorWrapper: '.quiz-notfound',
            optionsWrapper: '.quadro-options',
            optionsLine: '.quadro-options-line',
            optionsOption: '.quadro-option',

            gameEndPoints: '.pontuacao-final'
        },
        templates: {
            notFound: '<div class="row">\
                            <div class="col-xs-12">{{msg}}</div>\
                        </div>',
            quadroInfo: '<div class="quadro-image-wrapper">\
                            <a href="{{img}}" class="fluidbox">\
                                <img src="{{img}}" class="quadro-image" />\
                            </a>\
                        </div>\
                        <div class="quadro-info">\
                            <div class="quadro-titulo">{{title}}</div>\
                            <div class="quadro-ano">{{year}}</div>\
                            <div class="quadro-autor">{{author}}</div>\
                        </div>'
        }
    };

    function init(options) {
        that.options = $.extend(true, {}, default_options, options);

        that.pointsToWin = that.options.pointsPerCorrect;

        that.$quizWrapper = $(that.options.selectors.quizWrapper);
        that.$subTitle = $(that.options.selectors.subTitle);
        that.$quizInfo = $(that.options.selectors.quizInfo);
            that.$livesTooltip = $(that.options.selectors.quizLivesTooltip);
            that.$lives = $(that.options.selectors.quizLives);
            that.$pontos = $(that.options.selectors.quizPoints);
            that.$quadroContinente = $(that.options.selectors.quizContinente);
            that.$quadroAtual = $(that.options.selectors.quizAtual);
            that.$quadroTotal = $(that.options.selectors.quizTotal);
            that.$btnNext = $(that.options.selectors.btnNext);

        that.$successModal = $(that.options.selectors.successModal);

        that.$quadroWrapper = $(that.options.selectors.quadroWrapper);
        that.$errorWrapper = $(that.options.selectors.errorWrapper);

        that.$options = $(that.options.selectors.optionsOption);
        that.$optionLines = $(that.options.selectors.optionsLine);

        that.$optionsWrapper = $(that.options.selectors.optionsWrapper);

        that.$gameEndPoints = $(that.options.selectors.gameEndPoints);

        loadQuadro();
        triggers();
        
        return that;
    }

    function triggers() {
        that.$options.on('click', function (e) {
            e.preventDefault();
            if ($(this).hasClass('selected')) {
                
            } else {
                $(this)
                    .closest(that.options.selectors.optionsLine)
                    .find(that.options.selectors.optionsOption)
                    .removeClass('selected');
                $(this).addClass('selected');
            }

            isAnswersValid();
        });
        that.$btnNext.on('click', function (e) {
            e.preventDefault();
            
            isAnswersCorrect();
        });
        $(window).on('player.lives.down', function (e, data) {
            that.$livesTooltip.addClass('show');
            setTimeout(function () {
                that.$livesTooltip.removeClass('show');
            }, 800);
            updateQuizInfo();
        });
        $(window).on('player.lives.zero', function (e, data) {
           $(window).trigger('game.gameover', [window.player.points]); 
        });
        $(window).on('game.gameover', function (e, data) {
            resetQuiz();
            router.goTo('game-over');
            that.$gameEndPoints.text(data);
        });
        $(window).on('game.win', function (e, data) {
            resetQuiz();
            router.goTo('win');
            that.$gameEndPoints.text(data);
        });
    }

    function isAnswersCorrect() {
        var infos = window.json.quadros[that.atual].infos;
        var incorrect = false;

        that.$options.each(function() {
            var id = $(this).attr('data-id');

            if ($(this).hasClass('selected')) {
                if (infos[id] === 0) {
                    console.log(id, infos[id]);
                    incorrect = true;
                    return false;
                }
            }
        });

        if (incorrect === true) {
            incorrectAnswer(that.atual);
        } else {
            correctAnswer(that.atual);
        }
    }

    function incorrectAnswer(obj) {
        window.player.perdeVida();
        that.pointsToWin = that.pointsToWin - that.options.losePoints;
        tryAgain();
    }

    function tryAgain() {
        that.$subTitle.html(
            Mustache.render(that.options.tryAgainText, { pointsToWin: that.pointsToWin })
        );
        router.scrollTop();
    }

    function correctAnswer(obj) {
        window.player.ganhaPontos(that.pointsToWin);
        
        showSuccess();
        that.answered.push(obj);
        var next = getNextQuadroId();

        if (next !== false) {
            console.log('entrou aqui');
            that.$btnNext.removeClass('visible');
            that.step++;
            refreshQuadro(next);
        } else {
            console.log('depois aqui');
            $(window).trigger('game.win', [window.player.points]);
        }
        
        updateQuizInfo();
    }

    function getNextQuadroId() {
        var index = Math.floor(Math.random() * that.questions.length);

        if (that.answered.length < that.questions.length) {

            while (that.answered.hasOwnProperty(index)) {
                index = Math.floor(Math.random() * that.questions.length);
            }
            return index;
        } else {
            return false;
        }
    }

    function showSuccess() {
        that.$successModal.addClass('visible');
        resetForm();
        
        setTimeout(function() {
            that.$successModal.removeClass('visible');
        }, that.options.timeNextQuestion);
    }

    function isAnswersValid() {
        var invalid = false;

        that.$optionLines.each(function () {
            if ($(this).find(that.options.selectors.optionsOption + '.selected').length == 0) {
                that.$btnNext.removeClass('visible');
                invalid = true;
            }
        });

        if (!invalid) {
            that.$btnNext.addClass('visible');
            return true;
        } else {
            return false;
        }
    }
    
    function resetForm() {
        that.$options.removeClass('selected');
    }

    function resetQuiz() {
        $(window).trigger('player.points.reset');
        that.$successModal.removeClass('visible');
        that.state = 'idle';
        that.step = 1;
        that.atual = 0;
        resetForm();
        updateQuizInfo();
    }
    
    function loadQuadro(index) {
        $('body').removeClass('loading');
        var quadros = window.json.quadros;

        if (!quadros.length) {
            loadError('Nenhum quadro encontrado!');
        } else {
            var img, title, ano, autor;
            that.questions = [];
            if (!index) {
                that.questions = quadros.filter(function(el) {
                    return el.from == that.options.from
                });
                
                if (that.questions.length > 0) {
                    index = Math.floor(Math.random() * that.questions.length);
                } else {
                    loadError('Nenhum quadro encontrado!');
                }
            }
            
            if (that.questions.length > 0) {
                that.$quizWrapper.removeClass('notfound');
                that.state = 'active';
                refreshQuadro(index);
            } else {
                loadError('Nenhum quadro encontrado!');
            }
        }
    }

    function refreshQuadro(index) {
        that.pointsToWin = that.options.pointsPerCorrect;

        that.$subTitle.html(that.options.firstText);

        that.atual = index;
        that.$quadroWrapper.html(
            Mustache.render(that.options.templates.quadroInfo, {
                img: 'images/quadros/' + that.questions[index].img,
                title: that.questions[index].title,
                year: that.questions[index].year,
                author: that.questions[index].author
            })
        );
        $('.fluidbox').fluidbox();
        updateQuizInfo();
    }

    function updateQuizInfo() {
        if (that.state == 'active') {
            that.$quizInfo.show();
            var continente = window.json.home.continentes.filter(function(data) {
                return data.id == that.options.from;
            })[0].name;

            that.$quadroContinente.html(continente);
            that.$quadroAtual.html(that.step);
            that.$quadroTotal.html(that.questions.length);

            that.$lives.html(window.player.lives);
            that.$pontos.html(window.player.points);
        } else {
            that.$quizInfo.hide();
        }
    }

    function loadError(msg) {
        that.$errorWrapper.html(
            Mustache.render(that.options.templates.notFound, {
                msg: msg
            })
        );
        that.$quizWrapper.addClass('notfound');
        return false;
    }

    that.resetQuiz = resetQuiz;
    that.resetForm = resetForm;

    return init(options);
}
