function Router(options) {
    var that = {
        
    };
    var default_options = {
        selectors: {
            section: '.secao'
        }
    };

    function init(options) {
        that.options = $.extend(true, {}, default_options, options);
        
        if (!!window.location.hash) {
            goTo(window.location.hash);
        } else {
            goTo('home');
        }

        triggers();

        return that;
    }
    
    function triggers() {
        $(window).on('hashchange', function () {
            goTo(window.location.hash);
        });
    }

    function goTo(to, $sender, before) {
        $sender = typeof $sender !== 'undefined' ? $sender : '';
        before = typeof before !== 'undefined' ? before : '';

        if (!!to) {
            var toFormatted = to;
            if (to.indexOf('#') > -1) {
                toFormatted = to.substring(1);
            }

            if (!!before) {
                var confirma = confirm(before);
                if (!confirma) {
                    return false;
                }
            }

            console.log('>> ' + to);

            $(that.options.selectors.section).removeClass('visible');
            $(that.options.selectors.section + '.' + toFormatted).addClass('visible');
            scrollTop();

            if (!!$sender) {
                execute(toFormatted, $sender);
            } else {
                execute(toFormatted);
            }
        }
    }

    function scrollTop() {
        $('body').stop(0,1).animate({
            scrollTop: 0
        }, 300);
    }

    function execute(to, $sender) {
        switch (to) {
            case 'escolha':
                window.player = '';
                window.quiz = '';
                
                if (!!$sender) {
                    window.player = new Player();
                    window.quiz = new Quiz({
                        from: $sender.attr('data-id')
                    });
                } else {
                    window.location.hash = '#home';
                }
            break;
            default:
                if (!!window.quiz) {
                    window.quiz.resetQuiz();
                }
                window.home = new Home({
                    data: window.json.home
                });
        }
    }

    that.goTo = goTo;
    that.scrollTop = scrollTop;

    return init(options);
}