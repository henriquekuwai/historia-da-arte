function Home(options) {
    var that = {
        
    };
    var default_options = {
        title: 'História da Arte',
        data: {},
        selectors: {
            optionsList: '.home .homeOptions',
            homeTitle: '.home .homeTitle'
        },
        templates: {
            item: '<div class="row">\
                    <div class="col-xs-12">\
                        <a href="{{destino}}" class="box {{specialClass}}" data-id="{{id}}" style="background: #FFF url(images/bg/{{bg}}) no-repeat left top;">\
                            <h1>{{name}}</h1>\
                        </a>\
                    </div>\
                </div>'
        }
    };

    function init(options) {
        that.options = $.extend(true, {}, default_options, options);

        $('body').addClass('loading');
        that.$optionsList = $(that.options.selectors.optionsList);
        $(that.options.selectors.homeTitle).html(that.options.title);
        renderData(that.options.data);
        triggers();

        return that;
    }

    function triggers() {
        $('.continente').on('click', function (e) {
            e.preventDefault();
            $('body').addClass('loading');
            router.goTo($(this).attr('href'), $(this));
        });
    }

    function renderData(data) {
        if (!!data) {
            that.$optionsList.html(
                Mustache.render(
                    '{{#continentes}}' + that.options.templates.item + '{{/continentes}}',
                    data
                )
            );
        } else {
            console.warn('Data not found');
        }
        $('body').removeClass('loading');
    }

    that.renderData = renderData;

    return init(options);
}