function Player(options) {
    var that = {
        lives: 0,
        points: 0,
        name: 'Player'
    };
    var default_options = {
        data: [],
        maxLives: 3,
        initialPoints: 0,
        name: 'Player'
    };

    function init(options) {
        that.options = $.extend(true, {}, default_options, options);

        startPlayer();
        triggers();

        return that;
    }

    function triggers() {
        $(window).on('player.points.reset', function() {
            that.points = 0;
        });
    }

    function startPlayer() {
        that.lives = that.options.maxLives;
        that.points = that.options.initialPoints;
        that.name = that.options.name;
    }

    function ganhaPontos(qtd) {
        if (!!qtd && qtd > 0) {
            that.points += qtd;
        } else {
            return false;
        }
    }

    function perdeVida() {
        that.lives--;
        if (that.lives == 0) {
            $(window).trigger('player.lives.zero');
        } else {
            $(window).trigger('player.lives.down', [that.lives]);
        }
    }

    that.perdeVida = perdeVida;
    that.ganhaPontos = ganhaPontos;

    return init(options);
}